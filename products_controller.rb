class ProductsController < ApplicationController


  include StoresHelper
  before_action :authenticate_user!
  before_action :limit_access_sellers_only, only: [:new, :create, :owner_manage]
  before_action :limit_access_owner_only, only: [:edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:admin_manage, :admin_delete]

  def index
    @products = Product.all
  end


  def new
    @product = Product.new
  end


  def show
    set_product
  end


  def edit
    set_product
  end


  def create
    @product         = Product.new(product_params)
    @product.user_id = current_user.id
    if @product.save
      flash[:success] = 'Product has been created'
      redirect_to @product
    else
      flash.now[:danger] = 'Product has not been created'
      render :new
    end
  end


  def update
    set_product
    if @product.update(product_params)
      flash[:success] = 'Product has been updated'
      redirect_to @product
    else
      flash.now[:danger] = 'Product has not been updated'
      render :edit
    end
  end


  def destroy
    set_product
    if @product.destroy
      flash[:warning] = 'Product has been deleted'
      redirect_to products_path
    else
      flash.now[:danger] = 'Something went wrong'
      redirect_to @product
    end
  end


  # displays to Admin products of all users
  def admin_manage
    @products = Product.all.order("user_id DESC")
  end

  # allows to Admin to delete specific product
  def admin_delete
    set_product
    if @product.destroy
      flash[:warning] = 'Product has been deleted'
    else
      flash[:danger] = 'Somthing went wrong'
    end
    redirect_to products_admin_manage_path
  end

  # displays to seller his products and allows to edit this products
  def owner_manage
    @products = Product.all.where(user_id: current_user.id).order("created_at DESC")
  end


  private
    def product_params
      params.require(:product).permit(:name, :description, :price, :image, {:category_ids => []}, {:cause_ids => []})
    end

    def set_product
      @product = Product.find(params[:id])
    end


    def limit_access_owner_only
      @product = Product.find(params[:id])
      if current_user.id == @product.user_id
        flash[:success] = "Don't forget to save the changes"
      else
        redirect_to @product
        flash[:danger] = "Access Denied!"
      end
    end


    def limit_access_sellers_only
      if !is_seller? == true
        redirect_to products_path
      end
    end

end
