class Product < ActiveRecord::Base


  belongs_to :user, class_name: 'User'
  has_many :orders
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :causes


  validates :name, :description, :price, presence: true
  validates :price, numericality:{greater_than: 0}


  has_attached_file :image, :styles => { :medium => "200x>", :thumb => "100x100>" }, :default_url => "no_image.jpg"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

end
